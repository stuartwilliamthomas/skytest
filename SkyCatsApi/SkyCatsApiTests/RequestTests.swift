//
//  RequestTests.swift
//  SkyCatsApiTests
//
//  Created by Stuart Thomas on 04/02/2021.
//

import XCTest
@testable import SkyCatsApi

class RequestTests: XCTestCase {
  
  var subject: Request!
  
  func testWhenGoodDataIsReturnedTheDataIsParsed() throws {
    subject = Request(MockHttpServiceSuccess())
    let expectation = self.expectation(description: "request called")
    subject.fetchCatNews { catNews, error in
      expectation.fulfill()
      XCTAssertNil(error)
      XCTAssertNotNil(catNews)
    }
    waitForExpectations(timeout: 10, handler: nil)
  }
  
  func testWhenBadDataIsReturnedTheDataIsNotParsed() throws {
    subject = Request(MockHttpServiceBadData())
    let expectation = self.expectation(description: "request called")
    subject.fetchCatNews { catNews, error in
      expectation.fulfill()
      XCTAssertNotNil(error)
      XCTAssertNil(catNews)
    }
    waitForExpectations(timeout: 10, handler: nil)
  }
  
  func testWhenErrorIsReturnedTheDataIsNotParsed() throws {
    subject = Request(MockHttpServiceFailure())
    let expectation = self.expectation(description: "request called")
    subject.fetchCatNews { catNews, error in
      expectation.fulfill()
      XCTAssertNotNil(error)
      XCTAssertNil(catNews)
    }
    waitForExpectations(timeout: 10, handler: nil)
  }
  
}

