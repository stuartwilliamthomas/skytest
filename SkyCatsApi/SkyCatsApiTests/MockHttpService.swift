//
//  MockHttpService.swift
//  SkyCatsApiTests
//
//  Created by Stuart Thomas on 04/02/2021.
//

import XCTest
@testable import SkyCatsApi

class MockHttpServiceSuccess: HttpServiceType {
  func request(urlRequest: URLRequest, completion: @escaping Handler) {
    let json = """
    {
    "title": "Sky Cat News",
    "data": [
    {
      "id": "1",
      "type": "story",
      "headline": "Story Headline",
      "teaserText": "Story teaser text",
      "creationDate": "2020-11-18T00:00:00Z",
      "modifiedDate": "2020-11-19T00:00:00Z",
      "teaserImage": {
        "_links": {
          "url": {
            "href": "",
            "templated": true,
            "type": "image/jpeg"
          }
        },
        "accessibilityText": "Image content description"
      }
    },
    {
      "id": "2",
      "type": "story",
      "headline": "Story Headline",
      "teaserText": "Story teaser text",
      "creationDate": "2020-11-18T00:00:00Z",
      "modifiedDate": "2020-11-19T00:00:00Z",
      "teaserImage": {
        "_links": {
          "url": {
            "href": "",
            "templated": true,
            "type": "image/jpeg"
          }
        },
        "accessibilityText": "Image content description"
      }
    },
    {
      "type": "advert",
      "url": "advert/url"
    },
    {
      "id": "3",
      "type": "weblink",
      "headline": "Weblink headline",
      "weblinkUrl": "weblink url",
      "creationDate": "2020-11-18T00:00:00Z",
      "modifiedDate": "2020-11-19T00:00:00Z",
      "teaserImage": {
        "_links": {
          "url": {
            "href": "",
            "templated": true,
            "type": "image/jpeg"
          }
        },
        "accessibilityText": "Image content description"
      }
    },
    {
      "id": "4",
      "type": "story",
      "headline": "Story headline",
      "teaserText": "Story teaser text",
      "creationDate": "2020-11-18T00:00:00Z",
      "modifiedDate": "2020-11-19T00:00:00Z",
      "teaserImage": {
        "_links": {
          "url": {
            "href": "",
            "templated": true,
            "type": "image/jpeg"
          }
        },
        "accessibilityText": "Image content description"
      }
    },
    {
      "id": "5",
      "type": "weblink",
      "headline": "Weblink headline",
      "weblinkUrl": "",
      "creationDate": "2020-11-18T00:00:00Z",
      "modifiedDate": "2020-11-19T00:00:00Z",
      "teaserImage": {
        "_links": {
          "url": {
            "href": "",
            "templated": true,
            "type": "image/jpeg"
          }
        },
        "accessibilityText": "Image content description"
      }
    }
    ]
    }
    """.data(using: .utf8)
    completion(json, nil, nil)
  }
}

class MockHttpServiceFailure: HttpServiceType {
  func request(urlRequest: URLRequest, completion: @escaping Handler) {
    completion(nil, nil, MockError())
  }
}

class MockHttpServiceBadData: HttpServiceType {
  func request(urlRequest: URLRequest, completion: @escaping Handler) {
    let data = """
      {
        "test": "test"
      }
    """.data(using: .utf8)
    completion(data, nil, nil)
  }
}

class MockError: Error {
  var localizedDescription: String = "test"
}

