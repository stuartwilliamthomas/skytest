//
//  Request.swift
//  SkyCatsApi
//
//  Created by Stuart Thomas on 04/02/2021.
//

import Foundation

public protocol RequestType {
  typealias CatNewsHandler = (CatNews?, Error?) -> ()
  typealias CatNewsStoryHandler = (CatNewsStory?, Error?) -> ()
  func fetchCatNews(completion: @escaping CatNewsHandler)
  func fetchCatNewsStory(completion: @escaping CatNewsStoryHandler)
}

public class Request: RequestType {

  let httpService: HttpServiceType
  
  public init(_ httpService: HttpServiceType = HttpService()) {
    self.httpService = httpService
  }

  private var catNewsRequestURL: URL {
    return URL(string: "http://127.0.0.1:8000/sample_list.json")!
  }
  
  private var catNewsStoryRequestURL: URL {
    return URL(string: "http://127.0.0.1:8000/sample_story1.json")!
  }

  public func fetchCatNews(completion: @escaping CatNewsHandler) {
    let urlRequest = URLRequest(url: catNewsRequestURL)
    httpService.request(urlRequest: urlRequest) { [weak self] data, response, error in
      if error == nil {
        guard let data = data else {
          //here I would track that there has been some kind of error as data and error should not both be nil
          completion(nil, error)
          return
        }
        self?.handleCatNewsSuccess(completion, data)
      } else {
        // here I would track the error if it wasn't already being tracked API side
        completion(nil, error)
      }
    }
  }
  
  public func fetchCatNewsStory(completion: @escaping CatNewsStoryHandler) {
    let urlRequest = URLRequest(url: catNewsStoryRequestURL)
    httpService.request(urlRequest: urlRequest) { [weak self] data, response, error in
      if error == nil {
        guard let data = data else {
          //here I would track that there has been some kind of error as data and error should not both be nil
          completion(nil, error)
          return
        }
        self?.handleCatNewsStorySuccess(completion, data)
      } else {
        // here I would track the error if it wasn't already being tracked API side
        completion(nil, error)
      }
    }
  }

  private func handleCatNewsSuccess(_ completion: @escaping RequestType.CatNewsHandler,
                                    _ data: Data) {
    do {
      let decoder = JSONDecoder()
      decoder.keyDecodingStrategy = .convertFromSnakeCase
      let responseObjects = try decoder.decode(CatNews.self, from: data)
      completion(responseObjects, nil)
    } catch let parseError {
      // here I would track that there is a parse error
      completion(nil, parseError)
    }
  }
  
  private func handleCatNewsStorySuccess(_ completion: @escaping RequestType.CatNewsStoryHandler,
                                         _ data: Data) {
    do {
      let decoder = JSONDecoder()
      decoder.keyDecodingStrategy = .convertFromSnakeCase
      let responseObjects = try decoder.decode(CatNewsStory.self, from: data)
      completion(responseObjects, nil)
    } catch let parseError {
      // here I would track that there is a parse error
      completion(nil, parseError)
    }
  }

}
