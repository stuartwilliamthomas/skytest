//
//  CatNews.swift
//  SkyCatsApi
//
//  Created by Stuart Thomas on 04/02/2021.
//

import Foundation

// MARK: - CatNews
public struct CatNews: Codable {
    public let title: String
    public let data: [SingleCatNews]

    public init(title: String, data: [SingleCatNews]) {
        self.title = title
        self.data = data
    }
}

// MARK: - SingleCatNews
public struct SingleCatNews: Codable {
    public let id: String?
    public let type: String
    public let headline, teaserText: String?
    public let creationDate, modifiedDate: String?
    public let teaserImage: TeaserImage?
    public let url, weblinkURL: String?

    enum CodingKeys: String, CodingKey {
        case id, type, headline, teaserText, creationDate, modifiedDate, teaserImage, url
        case weblinkURL = "weblinkUrl"
    }

    public init(id: String?, type: String, headline: String?, teaserText: String?, creationDate: String?, modifiedDate: String?, teaserImage: TeaserImage?, url: String?, weblinkURL: String?) {
        self.id = id
        self.type = type
        self.headline = headline
        self.teaserText = teaserText
        self.creationDate = creationDate
        self.modifiedDate = modifiedDate
        self.teaserImage = teaserImage
        self.url = url
        self.weblinkURL = weblinkURL
    }
}

// MARK: - TeaserImage
public struct TeaserImage: Codable {
    public let links: Links
    public let accessibilityText: String

    enum CodingKeys: String, CodingKey {
        case links = "_links"
        case accessibilityText
    }

    public init(links: Links, accessibilityText: String) {
        self.links = links
        self.accessibilityText = accessibilityText
    }
}

// MARK: - Links
public struct Links: Codable {
    public let url: URLClass

    public init(url: URLClass) {
        self.url = url
    }
}

// MARK: - URLClass
public struct URLClass: Codable {
    public let href: String
    public let templated: Bool
    public let type: String

    public init(href: String, templated: Bool, type: String) {
        self.href = href
        self.templated = templated
        self.type = type
    }
}

