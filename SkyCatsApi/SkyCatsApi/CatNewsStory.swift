//
//  CatNewsStory.swift
//  SkyCatsApi
//
//  Created by Stuart Thomas on 05/02/2021.
//

import Foundation

// MARK: - CatNewsStory
public struct CatNewsStory: Codable {
    public let id, headline: String
    public let heroImage: HeroImage
    public let creationDate, modifiedDate: String
    public let contents: [Content]

    public init(id: String, headline: String, heroImage: HeroImage, creationDate: String, modifiedDate: String, contents: [Content]) {
        self.id = id
        self.headline = headline
        self.heroImage = heroImage
        self.creationDate = creationDate
        self.modifiedDate = modifiedDate
        self.contents = contents
    }
}

// MARK: - Content
public struct Content: Codable {
    public let type: String
    public let text, url, accessibilityText: String?

    public init(type: String, text: String?, url: String?, accessibilityText: String?) {
        self.type = type
        self.text = text
        self.url = url
        self.accessibilityText = accessibilityText
    }
}

// MARK: - HeroImage
public struct HeroImage: Codable {
    public let imageURL, accessibilityText: String

    enum CodingKeys: String, CodingKey {
        case imageURL = "imageUrl"
        case accessibilityText
    }

    public init(imageURL: String, accessibilityText: String) {
        self.imageURL = imageURL
        self.accessibilityText = accessibilityText
    }
}
