//
//  CatNewsStoryViewModelTests.swift
//  SkyTechTestTests
//
//  Created by Stuart Thomas on 05/02/2021.
//

import XCTest
@testable import SkyTechTest
@testable import SkyCatsApi

class CatNewsStoryViewModelTests: XCTestCase {
  
  var subject: CatNewsStoryViewModel!
  
  func testResultsAreReturnedWithASuccessfulRequest() throws {
    subject = CatNewsStoryViewModel(request: MockRequestSuccess())
    subject.fetchCatNewsStory { error in
      XCTAssertNil(error)
      XCTAssertNotNil(self.subject.result)
    }
  }
  
  func testErrorIsReturnedWithAFailedRequest() throws {
    subject = CatNewsStoryViewModel(request: MockRequestFailure())
    subject.fetchCatNewsStory { error in
      XCTAssertNotNil(error)
      XCTAssertNil(self.subject.result)
    }
  }
  
}
