//
//  MockCatNewsStory.swift
//  SkyTechTestTests
//
//  Created by Stuart Thomas on 05/02/2021.
//

import Foundation

import XCTest
@testable import SkyTechTest
@testable import SkyCatsApi

class MockCatNewsStory {
  
  static var catNewsStory: CatNewsStory {
    let content = Content(type: "", text: "", url: "", accessibilityText: "")
    let heroImage = HeroImage(imageURL: "", accessibilityText: "")
    return CatNewsStory(id: "",
                        headline: "",
                        heroImage: heroImage,
                        creationDate: "",
                        modifiedDate: "",
                        contents: [content])
  }
  
}
