//
//  MockRequestFailure.swift
//  SkyTechTestTests
//
//  Created by Stuart Thomas on 04/02/2021.
//

import XCTest
@testable import SkyTechTest
@testable import SkyCatsApi

class MockRequestFailure: RequestType {
  func fetchCatNewsStory(completion: @escaping CatNewsStoryHandler) {
    completion(nil, MockError())
  }
  
  func fetchCatNews(completion: @escaping CatNewsHandler) {
    completion(nil, MockError())
  }
}

class MockError: Error {
  var localizedDescription: String = "Test Error"
}
