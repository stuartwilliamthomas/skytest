//
//  MockRequestSuccess.swift
//  SkyTechTestTests
//
//  Created by Stuart Thomas on 04/02/2021.
//

import XCTest
@testable import SkyTechTest
@testable import SkyCatsApi

class MockRequestSuccess: RequestType {
  func fetchCatNews(completion: @escaping CatNewsHandler) {
    let results = CatNews(title: "Title", data: [MockCatNews.singleCatNews])
    completion(results, nil)
  }
  
  func fetchCatNewsStory(completion: @escaping CatNewsStoryHandler) {
    let results = MockCatNewsStory.catNewsStory
    completion(results, nil)
  }
}
