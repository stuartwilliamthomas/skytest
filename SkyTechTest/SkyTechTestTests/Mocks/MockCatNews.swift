//
//  MockCatNews.swift
//  SkyTechTestTests
//
//  Created by Stuart Thomas on 04/02/2021.
//

import Foundation

import XCTest
@testable import SkyTechTest
@testable import SkyCatsApi

class MockCatNews {
  
  static var singleCatNews: SingleCatNews {
    let urlClass = URLClass(href: "", templated: true, type: "image/jpeg")
    let links = Links(url: urlClass)
    let teaserImage = TeaserImage(links: links, accessibilityText: "Image content description")
    return SingleCatNews(id: "1",
                         type: "story",
                         headline: "Story Headline",
                         teaserText: "Story teaser text",
                         creationDate: "2020-11-18T00:00:00Z",
                         modifiedDate: "2020-11-19T00:00:00Z",
                         teaserImage: teaserImage,
                         url: "",
                         weblinkURL: "")
  }
  
}
