//
//  CatNewsViewModelTests.swift
//  SkyTechTestTests
//
//  Created by Stuart Thomas on 04/02/2021.
//

import XCTest
@testable import SkyTechTest
@testable import SkyCatsApi

class CatNewsViewModelTests: XCTestCase {
  
  var subject: CatNewsViewModel!
  
  func testResultsAreReturnedWithASuccessfulRequest() throws {
    subject = CatNewsViewModel(request: MockRequestSuccess())
    subject.fetchCatNews { error in
      XCTAssertNil(error)
      XCTAssertNotNil(self.subject.result)
    }
  }
  
  func testErrorIsReturnedWithAFailedRequest() throws {
    subject = CatNewsViewModel(request: MockRequestFailure())
    subject.fetchCatNews { error in
      XCTAssertNotNil(error)
      XCTAssertNil(self.subject.result)
    }
  }
  
}
