//
//  CatNewsCoordinator.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 04/02/2021.
//

import Foundation
import UIKit
import SkyCatsApi

class CatNewsCoordinator {
  
  weak var controller: UIViewController?
  
  init(controller: UIViewController) {
    self.controller = controller
  }
  
  func openCatNewsStory(for catNews: SingleCatNews) {
    let catNewsStoryStoryboard = UIStoryboard(name: "CatNewsStoryViewController", bundle: .main)
    guard let catNewsStoryController: CatNewsStoryViewController = catNewsStoryStoryboard.instantiateInitialViewController() as? CatNewsStoryViewController else { return }
    //catNewsStoryController.viewModel = CatNewsStoryViewModel(catNews: catNews) - This can be set when we have an API
    controller?.navigationController?.pushViewController(catNewsStoryController,
                                                         animated: true)
  }
  
}
