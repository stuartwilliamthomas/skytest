//
//  Date+IntervalFormat.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 04/02/2021.
//

import Foundation

extension String {
  
  static func formatDateInterval(dateString: String) -> String {
    let iso8601DateFormatter = ISO8601DateFormatter()
    iso8601DateFormatter.formatOptions = .withInternetDateTime
    let date = iso8601DateFormatter.date(from: dateString)
    
    let formatter = DateComponentsFormatter()
    formatter.unitsStyle = .brief
    
    formatter.allowedUnits = [.year, .day, .hour, .minute]
    
    guard let pickedDate = date else { return "" }
    let now = Date()
    let published = formatter.string(from: pickedDate, to: now)!
    return "\(published) ago"
  }
}
