//
//  CatNewsViewModel.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 04/02/2021.
//

import Foundation
import SkyCatsApi

class CatNewsViewModel {
  
  typealias Handler = (Error?) -> ()
  
  var request: RequestType?
  var result: CatNews?
  var title: String?
  var catNewsList: [SingleCatNews]?
  
  init(request: RequestType = Request()) {
    self.request = request
  }
  
  func fetchCatNews(completion: @escaping Handler) {
    request?.fetchCatNews { [weak self] result, error in
      self?.title = result?.title
      self?.result = result
      self?.catNewsList = result?.data
      completion(error)
    }
    
  }
  
}
