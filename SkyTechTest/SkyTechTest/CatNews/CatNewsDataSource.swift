//
//  CatNewsDataSource.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 04/02/2021.
//

import Foundation
import UIKit

class CatNewsDataSource: NSObject,
                         UICollectionViewDataSource,
                         UICollectionViewDelegate,
                         UICollectionViewDelegateFlowLayout {
  
  let collectionView: UICollectionView
  let catNewsCoordinator: CatNewsCoordinator
  var viewModel: CatNewsViewModel {
    didSet {
      collectionView.reloadData()
    }
  }
  
  init(_ collectionView: UICollectionView,
       _ viewModel: CatNewsViewModel,
       _ catNewsCoordinator: CatNewsCoordinator) {
    self.collectionView = collectionView
    self.viewModel = viewModel
    self.catNewsCoordinator = catNewsCoordinator
    super.init()
    collectionView.dataSource = self
    collectionView.delegate = self
    registerCell()
  }
  
  private func registerCell() {
    let standardCellDescription = String(describing: CatNewsCollectionViewCell.self)
    collectionView.register(UINib(nibName: standardCellDescription, bundle: nil),
                            forCellWithReuseIdentifier: standardCellDescription)
    let headlineCellDescription = String(describing: CatNewsHeadlineCollectionViewCell.self)
    collectionView.register(UINib(nibName: headlineCellDescription, bundle: nil),
                            forCellWithReuseIdentifier: headlineCellDescription)
  }
  
  //MARK: UICollectionViewDataSource
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.catNewsList?.count ?? 0
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if indexPath.row == 0 {
      let cellIdentifier = String(describing: CatNewsHeadlineCollectionViewCell.self)
      guard let headlineCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                                  for: indexPath) as? CatNewsHeadlineCollectionViewCell else {
        //track error here
        print(">>>>> error creating cat news headline cell")
        return UICollectionViewCell()
      }
      headlineCell.singleCatNews = viewModel.catNewsList?[indexPath.row]
      return headlineCell
    } else {
      let cellIdentifier = String(describing: CatNewsCollectionViewCell.self)
      guard let standardCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                                  for: indexPath) as? CatNewsCollectionViewCell else {
        //track error here
        print(">>>>> error creating standard cat news cell")
        return UICollectionViewCell()
      }
      standardCell.singleCatNews = viewModel.catNewsList?[indexPath.row]
      return standardCell
    }
  }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      guard let singleCatNews = viewModel.catNewsList?[indexPath.row] else { return }
      catNewsCoordinator.openCatNewsStory(for: singleCatNews)
    }
  
  //MARK: UICollectionViewDelegateFlowLayout
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    let cellHeight = indexPath.row == 0 ? 360 : 100
    let cellWidth = collectionView.frame.width - 16
    return CGSize(width: cellWidth, height: CGFloat(cellHeight))
  }
  
}
