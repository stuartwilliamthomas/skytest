//
//  CatNewsViewController.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 02/02/2021.
//

import UIKit

class CatNewsViewController: UIViewController {
  
  @IBOutlet weak var collectionView: UICollectionView!

  var dataSource: CatNewsDataSource?
  var viewModel: CatNewsViewModel!
  
  override func viewDidLoad() {
    viewModel = CatNewsViewModel()
    dataSource = CatNewsDataSource(collectionView, viewModel, CatNewsCoordinator(controller: self))
    super.viewDidLoad()
    fetchResults()
  }
  
  private func fetchResults() {
    viewModel?.fetchCatNews(completion: { [weak self] error in
      if error != nil {
        self?.showRequestError()
      } else {
        DispatchQueue.main.async {
          self?.title = self?.viewModel.title
          self?.collectionView.reloadData()
        }
      }
    })
  }
  
  private func showRequestError() {
    let alert = UIAlertController(title: "Something went wrong with your request",
                                  message: "Do you want to try again?",
                                  preferredStyle: UIAlertController.Style.alert)
    
    alert.addAction(UIAlertAction(title: "Try Again?",
                                  style: .default,
                                  handler: { [weak self] _ in
      self?.fetchResults()
    }))
    alert.addAction(UIAlertAction(title: "Cancel",
                                  style: .cancel))
    DispatchQueue.main.async {
      self.present(alert, animated: true, completion: nil)
    }
  }


}

