//
//  CatNewsCollectionViewCell.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 04/02/2021.
//

import Foundation
import UIKit
import SkyCatsApi

class CatNewsCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var teaserImage: UIImageView!
  @IBOutlet weak var headlineLabel: UILabel!
  @IBOutlet weak var publishedLabel: UILabel!
  @IBOutlet weak var teaserTextLabel: UILabel!
  
  var singleCatNews: SingleCatNews? {
    didSet {
      setupUI()
    }
  }
  
  private func setupUI() {
    guard let singleCatNews = singleCatNews else { return }
    teaserImage.image = retrieveImage()
    headlineLabel.text = singleCatNews.headline
    publishedLabel.text = String.formatDateInterval(dateString: singleCatNews.modifiedDate ?? "")
    
    teaserTextLabel.text = singleCatNews.teaserText
  }
  
  private func retrieveImage() -> UIImage {
    if let hrefString = singleCatNews?.teaserImage?.links.url.href,
       let url = URL(string: hrefString),
       let data = try? Data(contentsOf: url),
       let image = UIImage(data: data) {
      return image
    }
    
    return UIImage(named: "placeholder-image")!
  }
  
}
