//
//  CatNewsStoryParagraphCollectionViewCell.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 05/02/2021.
//

import Foundation
import UIKit
import SkyCatsApi

class CatNewsStoryParagraphCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var paragraphLabel: UILabel!
  
  var content: Content? {
    didSet {
      setupUI()
    }
  }
  
  private func setupUI() {
    guard let content = content else { return }
    paragraphLabel.text = content.text
  }
  
}
