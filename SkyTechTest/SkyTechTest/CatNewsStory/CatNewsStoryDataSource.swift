//
//  CatNewsStoryDataSource.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 05/02/2021.
//

import Foundation
import UIKit

class CatNewsStoryDataSource: NSObject,
                              UICollectionViewDataSource,
                              UICollectionViewDelegate,
                              UICollectionViewDelegateFlowLayout {
  
  let collectionView: UICollectionView
  let catNewsCoordinator: CatNewsCoordinator
  var viewModel: CatNewsStoryViewModel {
    didSet {
      collectionView.reloadData()
    }
  }
  
  init(_ collectionView: UICollectionView,
       _ viewModel: CatNewsStoryViewModel,
       _ catNewsCoordinator: CatNewsCoordinator) {
    self.collectionView = collectionView
    self.viewModel = viewModel
    self.catNewsCoordinator = catNewsCoordinator
    super.init()
    collectionView.dataSource = self
    collectionView.delegate = self
    registerCell()
  }
  
  private func registerCell() {
    let headlineCellDescription = String(describing: CatNewsStoryHeadlineCollectionViewCell.self)
    collectionView.register(UINib(nibName: headlineCellDescription, bundle: nil),
                            forCellWithReuseIdentifier: headlineCellDescription)
    let paragraphCellDescription = String(describing: CatNewsStoryParagraphCollectionViewCell.self)
    collectionView.register(UINib(nibName: paragraphCellDescription, bundle: nil),
                            forCellWithReuseIdentifier: paragraphCellDescription)
    let imageCellDescription = String(describing: CatNewsStoryImageCollectionViewCell.self)
    collectionView.register(UINib(nibName: imageCellDescription, bundle: nil),
                            forCellWithReuseIdentifier: imageCellDescription)
  }
  
  //MARK: UICollectionViewDataSource
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if let count = viewModel.result?.contents.count {
      return count + 1 // +1 to include headline and image
    }
    return 0
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if indexPath.row == 0 {
      let cellIdentifier = String(describing: CatNewsStoryHeadlineCollectionViewCell.self)
      guard let headlineCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                                  for: indexPath) as? CatNewsStoryHeadlineCollectionViewCell else {
        //track error here
        print(">>>>> error creating cat news story headline cell")
        return UICollectionViewCell()
      }
      headlineCell.catNewsStory = viewModel.result
      return headlineCell
    } else if viewModel.result?.contents[indexPath.row - 1].type == "paragraph" {
      let cellIdentifier = String(describing: CatNewsStoryParagraphCollectionViewCell.self)
      guard let paragraphCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                                   for: indexPath) as? CatNewsStoryParagraphCollectionViewCell else {
        //track error here
        print(">>>>> error creating cat news story paragraph cell")
        return UICollectionViewCell()
      }
      paragraphCell.content = viewModel.result?.contents[indexPath.row - 1]
      return paragraphCell
    } else {
      let cellIdentifier = String(describing: CatNewsStoryImageCollectionViewCell.self)
      guard let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                               for: indexPath) as? CatNewsStoryImageCollectionViewCell else {
        //track error here
        print(">>>>> error creating cat news story image cell")
        return UICollectionViewCell()
      }
      imageCell.content = viewModel.result?.contents[indexPath.row]
      return imageCell
    }
  }
  
  //MARK: UICollectionViewDelegateFlowLayout
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    let cellHeight = indexPath.row == 0 ? 360 : 100
    let cellWidth = collectionView.frame.width - 16
    return CGSize(width: cellWidth, height: CGFloat(cellHeight))
  }
  
}
