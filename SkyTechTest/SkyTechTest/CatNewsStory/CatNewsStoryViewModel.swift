//
//  CatNewsStoryViewModel.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 04/02/2021.
//

import Foundation
import SkyCatsApi

class CatNewsStoryViewModel {
  
  typealias Handler = (Error?) -> ()
  
  var request: RequestType?
  var result: CatNewsStory?
  // let product: Product - this can be set when we have an API
  
  init(request: RequestType = Request()) {
    self.request = request
  }
  
  func fetchCatNewsStory(completion: @escaping Handler) {
    request?.fetchCatNewsStory { [weak self] result, error in
      self?.result = result
      completion(error)
    }
    
  }
  
}

