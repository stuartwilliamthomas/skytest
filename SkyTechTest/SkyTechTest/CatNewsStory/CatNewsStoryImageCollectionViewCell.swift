//
//  CatNewsStoryImageCollectionViewCell.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 05/02/2021.
//

import Foundation
import UIKit
import SkyCatsApi

class CatNewsStoryImageCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var storyImage: UIImageView!
  
  var content: Content? {
    didSet {
      setupUI()
    }
  }
  
  private func setupUI() {
    storyImage.image = retrieveImage()
  }
  
  private func retrieveImage() -> UIImage {
    if let hrefString = content?.url,
       let url = URL(string: hrefString),
       let data = try? Data(contentsOf: url),
       let image = UIImage(data: data) {
      return image
    }
    
    return UIImage(named: "placeholder-image")!
  }
}
