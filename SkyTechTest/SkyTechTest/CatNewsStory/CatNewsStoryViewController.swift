//
//  CatNewsStoryViewController.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 05/02/2021.
//

import Foundation


import UIKit

class CatNewsStoryViewController: UIViewController {
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var dataSource: CatNewsStoryDataSource?
  var viewModel: CatNewsStoryViewModel!
  
  override func viewDidLoad() {
    viewModel = CatNewsStoryViewModel()
    dataSource = CatNewsStoryDataSource(collectionView, viewModel, CatNewsCoordinator(controller: self))
    super.viewDidLoad()
    fetchResults()
  }
  
  private func fetchResults() {
    viewModel?.fetchCatNewsStory(completion: { [weak self] error in
      if error != nil {
        self?.showRequestError()
      } else {
        DispatchQueue.main.async {
          self?.collectionView.reloadData()
        }
      }
    })
  }
  
  private func showRequestError() {
    let alert = UIAlertController(title: "Something went wrong with your search",
                                  message: "Do you want to try again?",
                                  preferredStyle: UIAlertController.Style.alert)
    
    alert.addAction(UIAlertAction(title: "Try Again?",
                                  style: .default,
                                  handler: { [weak self] _ in
      self?.fetchResults()
    }))
    alert.addAction(UIAlertAction(title: "Cancel",
                                  style: .cancel))
    DispatchQueue.main.async {
      self.present(alert, animated: true, completion: nil)
    }
  }


}

