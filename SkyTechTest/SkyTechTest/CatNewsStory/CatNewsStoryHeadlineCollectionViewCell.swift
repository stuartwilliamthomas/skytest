//
//  CatNewsStoryHeadlineCollectionViewCell.swift
//  SkyTechTest
//
//  Created by Stuart Thomas on 05/02/2021.
//

import Foundation
import UIKit
import SkyCatsApi

class CatNewsStoryHeadlineCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var headlineLabel: UILabel!
  @IBOutlet weak var headlineImage: UIImageView!
  
  var catNewsStory: CatNewsStory? {
    didSet {
      setupUI()
    }
  }
  
  private func setupUI() {
    headlineLabel.text = catNewsStory?.headline
    headlineImage.image = retrieveImage()
  }
  
  private func retrieveImage() -> UIImage {
    if let hrefString = catNewsStory?.heroImage.imageURL,
       let url = URL(string: hrefString),
       let data = try? Data(contentsOf: url),
       let image = UIImage(data: data) {
      return image
    }
    
    return UIImage(named: "placeholder-image")!
  }
  
  
}
