# Welcome!

Welcome to my Sky Tech Test. Sky has recently decided to move into the local cat news industry. To enable this, we need to build a prototype app to demonstrate to stakeholders. The basic premise of the app is to allow users to look at stories of cute cats nearby. 😻

## How to run app
#### Step1
Go to the /json directory and run 'python -m SimpleHTTPServer 8000'

### Step 2

Open the SkyTechTest.xcodeproj file in the SkyTechTest directory.

### Step 3

Run as usual.

## How to run tests

Tests run in the standard manor
